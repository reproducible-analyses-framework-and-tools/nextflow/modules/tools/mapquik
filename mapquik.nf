#!/usr/bin/env nextflow

process mapquik {
// Runs mapquik
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq1) - FASTQ 2
//   path(fa) - Reference FASTA
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: pafs
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.paf') - Output PAF File

// require:
//   FQS
//   FA
//   params.mapquik$mapquik_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'mapquik_container'
  label 'mapquik'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq)
  path fa
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path('*.paf'), optional: true, emit: pafs

  script:
  """
  mapquik ${fq} --reference ${fa} ${parstr} --threads ${task.cpus} -p ${dataset}-${run}
  """
}
